const config = {
  development: {
    UI_SERVER: 'http://localhost:3000',
    API_SERVER: 'http://hyno-external-rails-new-1353938241.ap-southeast-1.elb.amazonaws.com',
  },
  production: {
    UI_SERVER:'http://uat.tsbpass.telangana.gov.in',
    API_SERVER:
      'http://hyno-external-rails-new-1353938241.ap-southeast-1.elb.amazonaws.com',
  }
};

module.exports = config[process.env.NODE_ENV || 'development'];
