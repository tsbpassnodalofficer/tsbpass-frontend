export default {
  GROUNDFLOOR: 'Ground Floor',
  'GROUND+1FLOOR': 'Ground + 1 Upper Floor',
  'GROUND+2FLOORS': 'Ground + 2 Upper Floors',
  'GROUND+3FLOORS': 'Ground + 3 Upper Floors',
  'GROUND+4FLOORS': 'Ground + 4 Upper Floors',
  'GROUND+5FLOORSANDABOVE': 'Ground + 5 Upper Floors',
  'STILT+GROUNDFLOOR': 'Stilt for parking + Ground Floor',
  'STILT+GROUND+1FLOOR': 'Stilt for parking + Ground Floor + 1 Upper Floor',
  'STILT+GROUND+2FLOORS': 'Stilt for parking + Ground Floor + 2 Upper Floors',
  'STILT+GROUND+3FLOORS': 'Stilt for parking + Ground Floor + 3 Upper Floors',
  'STILT+GROUND+4FLOORS': 'Stilt for parking + Ground Floor + 4 Upper Floors',
  'STILT+GROUND+5FLOORSANDABOVE': 'Stilt for parking + Ground Floor + 5 Upper Floors',
  'MSB': 'MSB'
};

export const FLOOR_ORDER = {
  GROUND: 'Ground Floor',
  FIRST: 'First Floor',
  SECOND: 'Second Floor',
  THIRD: 'Third Floor',
  FOURTH: 'Fourth Floor',
  FIFTH: 'Fifth Floor',
  OTHERS: 'Others'
};

export const PLOTPART = {
  Approved: 'Approved Layout Plan',
  Ab: 'Approved Building Plan',
  Lrs: 'LRS Approved',
  Op: 'Open plot/Piece of land',
  Gk: 'Constructed prior to 1985',
  Lp: 'Gramakantam/ Abadi',
  Bps: 'Building regularized under BPS',
  Ot: 'Others',
  Unapproved: 'Unapproved Layout'
};

 export const SITE_AFFECTED_UNDER = {
  NalaWidening: 'Nala Widening',
  Tanks: 'Tanks',
  Kuntas: 'Kuntas',
  Canal: 'Canal',
  Vagus: 'Vagus',
  StormWater: 'Storm Water',
  Waterdrain: 'Water drain',
  BufferZone: 'Buffer Zone',
  FTL: 'FTL',
  NA: 'N/A'
};
