import floorDisplayNameConstants from '../floorDisplayNameConstants';
import { convertToSqYards } from '../../utils/convertionUtils';
import { get } from 'mobx';
export const getSelfDeclaration = applicationData => {
  var name = applicationData.name ? applicationData.name : 'I / We '
  var plot_area_sq_mtrs =
    applicationData.actual_plot_area < applicationData.plot_area_as_per_document
      ? applicationData.actual_plot_area
      : applicationData.plot_area_as_per_document;
  var plot_area_sq_yards = convertToSqYards(plot_area_sq_mtrs);

  //@brampr : This is the old set of self declaration statements as on 5th March 2020
  var selfDeclarationText = [
    'I / We <b>' + capitalize(name) + '</b> certify that the particulars furnished in the online application form and uploaded documents for registration as required under sub-section (1) of section 174 of the Telangana Municipalities Act 2019.',
    'Here by certify that size of the said plot is less than <b>' +
      plot_area_sq_mtrs +
      ' Sq. Mts. ( ' +
     parseInt(plot_area_sq_yards) +
      ' Sq.Yards )</b> and the plot is not part of bigger plot which has been split for this purpose and the proposed building is not more than <b>' +
      floorDisplayNameConstants[applicationData.no_of_floors] +
      '.</b>',
    'Here by certify that my plot is abutting to the existing road width of more than 9.00 mts. and if it is not available, I / We will surrender and fore go the affected portion to a strip of 4.5 mts. from the center of the existing road for road widening at free of cost.',
    'Here by undertake that I / We shall not construct any extra floor other than declared.',
    'We undertake that the proposed site is not a Government land / Prohibited Land / Disputed Land / Municipal Land / Layout Open Land / Water Bodies / Earmarked Parks / Playgrounds and not passing through High Tension/electricity lines and the proposal is in conformity with the Master plan Land use etc.',
    'Shall be made liable for penal action as proposed under section 177 and 180 of Telangana Municipalities Act, 2019 if in case of misrepresentation or false declaration.'
  ];

  if (plot_area_sq_mtrs <= 63) {
    selfDeclarationText = [
      'I / We <b>' + capitalize(name) +'</b> here by certify that <br/> we are the owners of the plot and that there is no other claimant or dispute pertaining to this plot.',
      'Here by certify that the extent/area of this plot is not more than <b>' +
        plot_area_sq_mtrs +
        ' Sq. Mts. ( ' +
        parseInt(plot_area_sq_yards) +
        ' Sq.Yards )</b>  and the plot is not part of bigger plot which has been split for this purpose.',
        getPointBasedOnCondution(applicationData.no_of_floors, plot_area_sq_mtrs),
      'Here by certify that I shall leave a front set-back of 5 feet from my plot boundry.',
      'Here by certify that my plot is abutting to the existing road width not less than 30 feet and if it is not available, I / We will maintain 15 feet distance from the center of the existing road in addition to front set-back.',
      'Here by certify that my proposed building is in confirmity with the master plan land use and zoning regulations.',
      'We undertake that the proposed site is not a Government land / Prohibited Land / Disputed Land / Municipal Land / Layout Open Land / Water Bodies / Earmarked Parks / Playgrounds and not passing through High Tension/electricity lines and the proposal is in conformity with the Master plan Land use etc.',
      'Shall be made liable for penal action as per Telangana Municipalities Act, 2019 if in case of misrepresentation or false declaration.',
      'Agree to include the registration fee of <b>' + getAmount(applicationData) +'</b> in my first property tax assessment.'
    ];
  } else if (plot_area_sq_mtrs > 63 && plot_area_sq_mtrs <= 200) {
    selfDeclarationText = [
      'I / We <b>' + capitalize(name) +'</b> here by certify that <br/> we are the owners of the plot and that there is no other claimant or dispute pertaining to this plot.',
      'Here by certify that the extent/area of this plot is not more than  <b>' +
        plot_area_sq_mtrs +
        ' Sq. Mts. ( ' +
        parseInt(plot_area_sq_yards) +
        ' Sq.Yards )</b> and height of the proposed building is not more than 7mts (<b>' +
        floorDisplayNameConstants[applicationData.no_of_floors] +
        // '</b>) and the plot is not part of bigger plot which has been split for this purpose.',
        getPointBasedOnCondution(applicationData.no_of_floors, plot_area_sq_mtrs),
      'Here by certify that I / We shall construct the building leaving the mandatory set backs, road affected area and as per National Building Code under the supervision of qualified technical person.',
      'Here by certify that my plot is abutting to the existing road width not less than 30 feet and if it is not available, I will maintain 15 feet distance from the center of the existing road in addition to front set-back.',
      'Here by undertake that the proposed site is not a Government land / Prohibited land / Disputed land / Municipal land / Layout Open land / Water bodies / Earmarked Parks and playgrounds and the proposal is in confirmity with the master plan land use etc.,',
      'Shall be made liable for penal action ie., Penality, Revokation, Demolition with out notice as per Telangana Municipalities Act, 2019 if in case of misrepresentation or false declaration.',
      'Certify that I / We will construct the <b>1<b> rain water harvesting pit, and will plant <b>6</b> Trees with in the plot at appropriate location.',
    ];
  } else if (plot_area_sq_mtrs > 200 && plot_area_sq_mtrs <= 500) {
    selfDeclarationText = [
      'I / We <b>' + capitalize(name) +'</b> here by certify that <br/> we are the owners of the plot and that there is no other claimant or dispute pertaining to this plot.',
      'Here by certify that the extent/area of this plot is not more than <b>' +
        plot_area_sq_mtrs +
        ' Sq. Mts. ( ' +
        plot_area_sq_yards +
        ' Sq.Yards )</b> and height of the proposed building is not more than 10mts (<b>' +
        floorDisplayNameConstants[applicationData.no_of_floors] +
        // '</b>) and the plot is not part of bigger plot which has been split for this purpose.',
        getPointBasedOnCondution(applicationData.no_of_floors, plot_area_sq_mtrs),
      'Here by certify that I / We shall construct the building leaving the mandatory set backs, road affected area and as per National Building Code under the supervision of qualified technical person.',
      'Here by certify that my plot is abutting to the existing road width not less than 30 feet and if it is not available, I / We will maintain 15 feet distance from the center of the existing road in addition to front set-back.',
      'We undertake that the proposed site is not a Government land / Prohibited Land / Disputed Land / Municipal Land / Layout Open Land / Water Bodies / Earmarked Parks / Playgrounds   and not passing through High Tension/electricity lines and the proposal is in conformity with the Master plan Land use etc.',
      'Shall be made liable for penal action ie., Penality, Revokation, Demolition with out notice as per Telangana Municipalities Act, 2019 if in case of misrepresentation or false declaration.',
    ];
  } else if (plot_area_sq_mtrs > 500) {
    selfDeclarationText = [
      'I / We <b>' + capitalize(name) +'</b> here by certify that <br/> we are the owners of the plot and that there is no other claimant or dispute pertaining to this plot.',
      'Here by undertake that I shall not construct more than <b>' +
        floorDisplayNameConstants[applicationData.no_of_floors] +
        '</b>. Any further construction beyond Ground + First Floor will have prior approvals failing which I / We will be liable for penality / Demolition with out notice.',
      'Here by certify that I / We shall construct the building leaving the mandatory set backs, road affected area and as per National Building Code under the supervision of qualified technical person.',
      'Here by certify that my plot is abutting to the existing road width not less than 30 feet and if it is not available, I / We will maintain 15 feet distance from the center of the existing road in addition to front set-back.',
      'I / We undertake that the proposed site is not a Government land / Prohibited Land / Disputed Land / Municipal Land / Layout Open Land / Water Bodies / Earmarked Parks / Playgrounds and not passing through High Tension/electricity lines and the proposal is in conformity with the Master plan Land use etc.',
      'Shall be made liable for penal action ie., Penality, Revokation, Demolition with out notice as per Telangana Municipalities Act, 2019 if in case of misrepresentation or false declaration.',
    ];
  }

  if (applicationData.front_setback > 0 || applicationData.other_setback > 0)
    selfDeclarationText.push(
      'I / We will leave the required set backs as applicable to my plot during my construction, as prescribed in the application section.'
    );

  if (applicationData.is_under_slum_area == 'true')
    selfDeclarationText.push(
      'I / We will forgo the plot land if required by the Government for the development of the slum area the plot falls in.'
    );

    selfDeclarationText.push(
      'Declare that if I / We violate above declarations the authority is entitled to demolish such construction and all unauthorized or deviated portions unconditionally without any prior notice to Me / Us. Further , I / We shall be held responsible and liable for all penal consequences as per the provisions of Telangana Municipalities Act 2019'
      );  
  return selfDeclarationText;
};

const getPointBasedOnCondution = (no_of_floors, plotArea) => {
if(no_of_floors && plotArea) {
  if(((plotArea > 300 && plotArea <= 500) && 
  (no_of_floors === 'STILT+GROUND+3FLOORS' || no_of_floors === 'GROUND+3FLOORS')) || plotArea > 500) {
    return 'Please check &verify any construction without having prior approval I / We will be liable for penalty/demolition without notice.'
  }
}
return 'Here by undertake that I / We shall not construct more than <b>' +
floorDisplayNameConstants[no_of_floors] +
'</b>. Any further construction beyond <b>' +
floorDisplayNameConstants[no_of_floors] +
'</b> will have prior approvals failing which I / We will be liable for penality / Demolition with out notice.'
}

const capitalize = (s) => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

const getAmount = (property) => {
  const onlineReg = property.is_road_widening && property.is_road_widening === 'true'  ? true :
  property.site_affected_under && property.site_affected_under !=='NA' ? true:
  property.sub_division && property.sub_division === 'true'? true:
  property.construction_type && property.construction_type === 'NON_RESIDENTIAL'? true:
  property.plot_status && property.plot_status === 'Unapproved' || property.plot_status === 'Op'
  ? true : false;
  if(onlineReg) {
    return 'TEN THOUSAND'
  }
  return 'ONE RUPEE'
}

