import React, { useState, useEffect } from 'react';
import { Form } from 'react-bootstrap';
import { AngleBottom, UploadFile } from '../../utils/icons';
import { getTranslatedText } from '../../utils/translationUtils';
import QuestionsInput from '../../components/common/question-input'
import FileUpload from '../../components/common/file-upload'

import axios from 'axios';
import apiConstants from '../../constants/apiConstants';
const apiURL = require('../../config/url-front').API_SERVER;

import { plotPlanData } from '../../constants/data/plotPlanData'
import ContextApi from '../../context'

const TaxDetails = ({
    questions,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    customHandleChange
}) => {
    
  const [searchFour, setSearchFour] = useState(false);
  const [plan, setPlan] = useState({id:0, name: ''});
  const [caseType, setCaseType] = useState("NEW");
  
  const iconClickFour = () => {
    setSearchFour(!searchFour)
  }

  const handleQuestionAns = (id, value) => {
    const data = values.questionAnswers;
    var index = 0;
    data.some(function(question, i) {
        if (question.question_id === id) {
            index = i + 1;
            return true;
        }
    });
    
    if(index === 0){
      data.push({
        "question_id": id,
        "answer": value
      })
    }else{
      data[index - 1].answer = value 
    }

    customHandleChange('questionAnswers', data)
  }

  return (
    <ContextApi.Consumer>
      {value =>
    <>
        <div className="border-line-bottom">
            <span></span>
          </div>
          <h5 className="form-title">{getTranslatedText('heading.documents_tax_details')}</h5>
          
          {/* <Form.Group>
            <Form.Label>{getTranslatedText('heading.case_type')}</Form.Label>
            <div className="property-radio building-box">
              <Form.Check
                type="radio"
                label="New"
                name="casetype"
                id="buildingcase"
                value="NEW"
                defaultChecked={true}
                onChange={(e) => { setCaseType(e.target.value), value.handleContextCaseType(e.target.value)}}
              />
              <Form.Check
                type="radio"
                label="Additional"
                name="casetype"
                id="buildingtype"
                value="ADDITIONAL"
                onChange={(e) => { setCaseType(e.target.value), value.handleContextCaseType(e.target.value)}}
              />
            </div>
          </Form.Group> */}
          
        <Form.Group>
          <Form.Label>Legal Proof of ownership (Sale Deed)</Form.Label>
          <FileUpload 
            Label="Legal Proof of ownership (Sale Deed)"
            ID={`legalProof`}
            Filename={`legalProof`}
            FileType={`legal_proof_of_ownership`}
            errors={errors}
            required
            touched={touched}
            handleBlur={handleBlur}
            customHandleChange={(name, value) => customHandleChange('legalProof', value)}
          />
        </Form.Group>
          
          {questions.length > 0 &&
            questions.map((item, index) =>
              <QuestionsInput 
                key={`questions-${index}`} 
                item={item}
                errors={errors}
                touched={touched}
                handleBlur={handleBlur}
                customHandleChange={(id, ans) => handleQuestionAns(id, ans)}
               />
            )
          }
    </>
    }
    </ContextApi.Consumer>
  )
}


export default TaxDetails;