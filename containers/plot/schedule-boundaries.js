import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { Form, Button, Col, InputGroup, FormControl } from 'react-bootstrap';
import { AngleRight, AngleBottom, MapIcon, AngleLeft, UploadFile } from '../../utils/icons';
import { getTranslatedText } from '../../utils/translationUtils';
import InputGroupPrepend from '../../components/common/input-group'

const ScheduleBoundaries = ({
    values,
    errors,
    touched,
    handleChange,
    handleBlur
}) => {
    const [north, setNorth] = useState(3)
    const [south, setSouth] = useState(3)
    const [east, setEast] = useState(3)
    const [west, setWest] = useState(3)
  return (
    <>
        <div className="border-line-bottom">
            <span></span>
          </div>
          <h5 className="form-title">{getTranslatedText('heading.schedule_of_boundaries')}</h5>
          <Form.Group>
            <Form.Label>{getTranslatedText('label.boundary_schedule_north')}</Form.Label>
            <div className="property-radio">
              <Form.Check
                type="radio"
                label={getTranslatedText('label.plot_label')}
                name="yes-no2"
                id="property5"
                defaultChecked={north}
                onClick={() => setNorth(1)}
              />
              <Form.Check
                type="radio"
                label={getTranslatedText('label.road_label')}
                name="yes-no2"
                id="property6"
                defaultChecked={north}
                onClick={() => setNorth(2)}
              />
              <Form.Check
                type="radio"
                label={getTranslatedText('label.open_land')}
                name="yes-no2"
                id="property7"
                defaultChecked={north}
                onClick={() => setNorth(3)}
              />
            </div>
          </Form.Group>
          {north === 1?
          <InputGroupPrepend
            InputGroupName="in sq. yards"
            Input="northPlotWidth"
            Label="label.north_plot_area"
            ConvertYards={true}
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />: north === 2?
          <InputGroupPrepend
            InputGroupName="In Feets"
            Input="northRoadwidth"
            Label="label.north_road_width"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />:'' }

          <Form.Group>
            <Form.Label>{getTranslatedText('label.boundary_schedule_south')}</Form.Label>
            <div className="property-radio">
              <Form.Check
                type="radio"
                label={getTranslatedText('label.plot_label')}
                name="yes-no3"
                id="property8"
                defaultChecked={south}
                onClick={() => setSouth(1)}
              />
              <Form.Check
                type="radio"
                label={getTranslatedText('label.road_label')}
                name="yes-no3"
                id="property9"
                defaultChecked={south}
                onClick={() => setSouth(2)}
              />
              <Form.Check
                type="radio"
                label={getTranslatedText('label.open_land')}
                name="yes-no3"
                id="property10"
                defaultChecked={south}
                onClick={() => setSouth(3)}
              />
            </div>
          </Form.Group>
          {south === 1?
          <InputGroupPrepend
            InputGroupName="in sq. yards"
            Input="southplotWidth"
            Label="label.south_plot_area"
            ConvertYards={true}
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />: south === 2?
          <InputGroupPrepend
            InputGroupName="In Feets"
            Input="southRoadWidth"
            Label="label.south_road_width"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />:'' }

          <Form.Group>
            <Form.Label>{getTranslatedText('label.boundary_schedule_east')}</Form.Label>
            <div className="property-radio">
              <Form.Check
                type="radio"
                label={getTranslatedText('label.plot_label')}
                name="yes-no4"
                id="property11"
                defaultChecked={east}
                onClick={() => setEast(1)}
              />
              <Form.Check
                type="radio"
                label={getTranslatedText('label.road_label')}
                name="yes-no4"
                id="property12"
                defaultChecked={east}
                onClick={() => setEast(2)}
              />
              <Form.Check
                type="radio"
                label={getTranslatedText('label.open_land')}
                name="yes-no4"
                id="property13"
                defaultChecked={east}
                onClick={() => setEast(3)}
              />
            </div>
          </Form.Group>
          {east === 1?
          <InputGroupPrepend
            InputGroupName="in sq. yards"
            Input="eastPlotWidth"
            Label="label.ease_plot_area"
            ConvertYards={true}
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />: east === 2?
          <InputGroupPrepend
            InputGroupName="In Feets"
            Input="eastRoadWidth"
            Label="label.ease_road_width"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />:'' }
          
          <Form.Group>
            <Form.Label>{getTranslatedText('label.boundary_schedule_west')}</Form.Label>
            <div className="property-radio">
              <Form.Check
                type="radio"
                label={getTranslatedText('label.plot_label')}
                name="yes-no5"
                id="property14"
                defaultChecked={west}
                onClick={() => setWest(1)}
              />
              <Form.Check
                type="radio"
                label={getTranslatedText('label.road_label')}
                name="yes-no5"
                id="property15"
                defaultChecked={west}
                onClick={() => setWest(2)}
              />
              <Form.Check
                type="radio"
                label={getTranslatedText('label.open_land')}
                name="yes-no5"
                id="property16"
                defaultChecked={west}
                onClick={() => setWest(3)}
              />
            </div>
          </Form.Group>
          {west === 1?
          <InputGroupPrepend
            InputGroupName="in sq. yards"
            Input="westPlotWidth"
            Label="label.west_plot_area"
            ConvertYards={true}
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />: west === 2?
          <InputGroupPrepend
            InputGroupName="In Feets"
            Input="westRoadWidth"
            Label="label.west_road_width"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />:'' }
          
    </>
  )
}


export default ScheduleBoundaries;