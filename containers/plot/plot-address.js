import React, { useState, useEffect } from 'react';
import { getTranslatedText } from '../../utils/translationUtils';
import InputGroupPrepend from '../../components/common/input-group'

import ContextApi from '../../context'

const PlotAddress = ({
    values,
    errors,
    touched,
    handleChange,
    handleBlur
}) => {

  return (
    <ContextApi.Consumer>
      {value =>
        <>
            <div className="border-line-bottom">
                <span></span>
            </div>
            <h5 className="form-title">{getTranslatedText('label.plot_area')}</h5>
            <InputGroupPrepend
                InputGroupName="in sq. yards"
                Input="plotArea"
                Label="label.plot_area_document"
                ConvertYards={true}
                values={values}
                errors={errors}
                touched={touched}
                handleChange={handleChange}
                handleBlur={handleBlur}
            />
            <InputGroupPrepend
                InputGroupName="in sq. yards"
                Input="plotAreaGround"
                Label="label.plot_area_ground"
                ConvertYards={true}
                values={values}
                errors={errors}
                touched={touched}
                handleChange={handleChange}
                handleBlur={handleBlur}
            />
        </>
      }
    </ContextApi.Consumer>
  )
}


export default PlotAddress;