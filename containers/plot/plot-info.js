import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { Form, Button, Col, InputGroup, FormControl } from 'react-bootstrap';
import { AngleRight, AngleBottom, MapIcon, AngleLeft, UploadFile } from '../../utils/icons';
import { getTranslatedText } from '../../utils/translationUtils';
import MapModal from '../../components/application/MapModal'
import useDebounce from '../../components/common/use-debounce';
import InputGroupPrepend from '../../components/common/input-group'
import axios from 'axios';
import apiConstants from '../../constants/apiConstants';
const apiURL = require('../../config/url-front').API_SERVER;


const PlotInfo = ({
    houseType,
    localityType,
    surveyType,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    customHandleChange
}) => {
    const [modalShow, setModalShow] = useState(false);
    const [village, setVillage] = useState(false);
    // State and setter for search term
    const [villageTerm, setVillageTerm] = useState('');
    // State and setter for search results
    const [villages, setVillages] = useState([]);
    // State for search status (whether there is a pending API request)
    const [villageSearching, setVillageSearching] = useState(false);
    const [villageOpen, setVillageOpen] = useState(false);
    const [noRecords, setNoRecords] = useState(false);
    const [mandalZone, setMandalZone] = useState();
    const [municiPality, setMuniciPality] = useState();
  
    const debouncedVillageSearchTerm = useDebounce(villageTerm, 500);
    
  useEffect(() => {
    const authToken = localStorage.getItem('auth-token');

    if (debouncedVillageSearchTerm && !villageOpen) {
      // Set isSearching state
      setVillageSearching(true);
      setVillageOpen(true)
      // Fire off our API call
      axios.get(apiURL + apiConstants.SEARCH_VILLAGE.URL, {
        headers: { 'Authorization': 'Bearer ' + authToken },
        params: {
          query: debouncedVillageSearchTerm
        }
      })
        .then(function (response) {
          // console.log(response);
          setVillageSearching(false);
          // Set results state
          if(response.data.data.results.length > 0)
            setVillages(response.data.data.results);
          else
            setNoRecords('No Records found !!')
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      setVillages([]);
      setNoRecords(false);
      setVillageOpen(false)
    }

    if (!debouncedVillageSearchTerm){
      customHandleChange('villageName', "");
      customHandleChange('mandalZone', "");
      customHandleChange('municiPality', "");
      setVillageOpen(false)
    }

  }, [debouncedVillageSearchTerm])

  const selectVillage = (item) => {
    setVillage(item);
    customHandleChange('locationId', item.id);
    customHandleChange('villageName', item.village);
    customHandleChange('mandalZone', item.village);
    customHandleChange('municiPality', item.village);
    setVillageTerm(item.village);
    // setMandalZone(item.mandal)
    // setMuniciPality(item.ulb_name)
    setTimeout(() => {
      setVillageOpen(false)
    }, 1000)
  }

  const getGeoCoordinates = (values) => {
      customHandleChange('geoLocation', values);
  }
  return (
    <>
        <h5 className="form-title">{getTranslatedText('heading.plot_address')}</h5>
        
        <InputGroupPrepend
            List={houseType}
            Dropdown={true}
            InputGroupName="plotType"
            Input="plotNo"
            Label="label.plot_no"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />
        
        <InputGroupPrepend
            List={['Street', 'Road']}
            Dropdown={true}
            InputGroupName="streetType"
            Input="streetName"
            Label="label.street"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />

        <InputGroupPrepend
            List={localityType}
            Dropdown={true}
            InputGroupName="localityType"
            Input="localityName"
            Label="label.locality"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />

        <InputGroupPrepend
            List={surveyType}
            Dropdown={true}
            InputGroupName="surveyType"
            Input="surveyName"
            Label="label.survey_no"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />
             
        <Form.Group controlId="ControlSelect1">
        <Form.Label>{getTranslatedText('label.village_circle')}</Form.Label>
        <div className={`selected-box ${villageOpen ? 'open' : ''}`}>
            <div className="village-box">
            <Form.Control 
              type="type" 
              name="villageName"
              value={villageTerm} 
              onChange={(e) => setVillageTerm(e.target.value)} 
              placeholder="Select" 
              autoComplete="off" 
              className={errors.villageName && touched.villageName && !villageTerm && 'has-error'}
              onBlur={handleBlur}  />
              {errors.villageName && touched.villageName && !villageTerm && <p>{errors.villageName}</p>}
            <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleBottom }} />
            </div>
            <div className="search-list-box">
            <ul>
                {villageSearching && <li>Searching...</li>}
                {noRecords && <li>{noRecords}</li>}
                {villages.length > 0 ?
                villages.map((item, i) =>
                    <li key={i} onClick={() => selectVillage(item)}>{item.village}, {item.mandal}, {item.ulb_name}</li>
                ) : ''}
            </ul>
            </div>
        </div>
        </Form.Group>
        <Form.Group>
            <Form.Label>{getTranslatedText('label.mandal_zone')}</Form.Label>
            <Form.Control type="text" value={values.mandalZone} readOnly />
        </Form.Group>
        <Form.Group>
            <Form.Label>{getTranslatedText('label.municipality_corporation')}</Form.Label>
            <Form.Control type="text" value={values.municiPality} readOnly />
        </Form.Group>
        <Form.Group>
            <Form.Label>{getTranslatedText('heading.plot_address')}</Form.Label>
            <Form.Control 
              as="textarea" 
              rows="3"
              name="plotAddress"
              className={errors.plotAddress && touched.plotAddress && 'has-error'}
              placeholder="Enter Plot Address" 
              onChange={(e) => handleChange(e)}
              onBlur={handleBlur}
              value={values.plotAddress} />
              {errors.plotAddress && touched.plotAddress && <p>{errors.plotAddress}</p>}
        </Form.Group>
        <Form.Group>
          <Form.Label>{getTranslatedText('label.geo_coordinates')} <span> (Click on the map icon and select the area)</span></Form.Label>
          <div className="coordinates-box">
              <Form.Control 
                type="text" 
                placeholder="123.234, 1224, 3354" 
                name="geoLocation"
                className={errors.geoLocation && touched.geoLocation && 'has-error'}
                onChange={handleChange}
                onBlur={handleBlur} 
                value={values.geoLocation} />
                {errors.geoLocation && touched.geoLocation && <p>{errors.geoLocation}</p>}
              <svg className="arrow-right" onClick={() => setModalShow(true)} dangerouslySetInnerHTML={{ __html: MapIcon }} />
              <span className="map-error-message">Please select the plot geo coordinates to continue</span>
          </div>
        </Form.Group>
        <MapModal
            show={modalShow}
            onHide={() => setModalShow(false)}
            getCoordinates={getGeoCoordinates}
        /> 
    </>
  )
}


export default PlotInfo;