import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { Form, Button, Col, InputGroup, FormControl } from 'react-bootstrap';
import { AngleRight, AngleBottom, MapIcon, AngleLeft, UploadFile } from '../../utils/icons';
import { getTranslatedText } from '../../utils/translationUtils';
import InputGroupUnits from '../../components/common/input-group-units'

import ContextApi from '../../context';

const BuiltupArea = ({
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    customHandleChange
}) => {

  const [floorsData, setFloorsData] = useState([])

  const handleFloors = (id, type, value, floorId, floorType) => {
    // console.log('values', id, type, value)
    // return;

    const data = floorsData;
    var index = 0;
    data.some(function(question, i) {
        if (question.id === id) {
            index = i + 1;
            return true;
        }
    });
    
    if(index === 0){
      if(type === 'Qty'){
        data.push({
          "id": id,
          "floor_id": floorId,
          "floor_type": floorType,
          "units": parseInt(value)
        })
      }else{
        data.push({
          "id": id,
          "floor_id": floorId,
          "floor_type": floorType,
          "area": parseInt(value)
        })
      }
    }else{
      
      if(type === 'Qty'){
        data[index - 1].units = value 
      }else{
        data[index - 1].area = value 
      }
    }
    setFloorsData(data)
    
    customHandleChange('builtupArea', data)
    totalBuiltupUnits()
  }

  const totalBuiltupUnits = () => {
    // calculateSum(floorsData, area)
    let totalBuiltupArea = 0;
    let totalBuiltupUnits = 0;
    floorsData.forEach( data => {
      totalBuiltupArea = totalBuiltupArea + parseInt(data.area)
      totalBuiltupUnits = totalBuiltupUnits + parseInt(data.units)  
    });

    customHandleChange('prBuiltup', totalBuiltupArea)
    customHandleChange('prBuiltupUnits', totalBuiltupUnits)
    customHandleChange('mortgageArea', (parseInt(totalBuiltupArea) * 10 / 100))
  }


  const displayFloors = (floorId, floors, floorType) => {
    var indents = [];
    for (var i = 0; i < floors; i++) {
      indents.push(<InputGroupUnits
        InputGroupName="sq. Yards"
        Input={`Floors${i}`}
        Units={`Units${i}`}
        Label={`label.${i === 0 ? floorType : 'floor_'+i}`}
        KeyValue={i}
        values={values}
        errors={errors}
        touched={touched}
        handleChange={handleChange}
        handleBlur={(e, key) => { handleFloors(key, e.target.placeholder, e.target.value, floorId, floorType)}} 
      />);
    }
    return indents;
  }

  return (
    <ContextApi.Consumer>
      {value =>
        <>
        <h5 className="form-title">Proposed {getTranslatedText('title.builtup_area')}</h5>

            {displayFloors(value.floorsID, value.noOfFloors, value.foolrsType)}

            
            <InputGroupUnits
              InputGroupName="sq. Yards"
              Input="prBuiltup"
              Units="prBuiltupUnits"
              Label="label.builtup_total"
              readOnly={true}
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
            />
        </>
      }
    </ContextApi.Consumer>
  )
}


export default BuiltupArea;