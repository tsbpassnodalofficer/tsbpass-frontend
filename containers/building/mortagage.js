import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { Form, Button, Col, InputGroup, FormControl } from 'react-bootstrap';
import { AngleRight, AngleBottom, MapIcon, AngleLeft, UploadFile } from '../../utils/icons';
import { getTranslatedText } from '../../utils/translationUtils';
import InputGroupPrepend from '../../components/common/input-group'

const Mortagage = ({
    values,
    errors,
    touched,
    handleChange,
    handleBlur
}) => {

  const [floorOpen, setFloorOpen] = useState(false);
  const [floors, setFloors] = useState([]);
  const [searchFive, setSearchFive] = useState(false);

  const iconClick = () => {
    setFloorOpen(!floorOpen)
  }
  const iconClickFive = () => {
    setSearchFive(!searchFive)
  }

  const floorsData = ['Floor1', 'Floor2', 'Floor3', 'Floor4']

  const handleFloorsSelection = (item) => {
    const index = floors.indexOf(item);
    if (index > -1) {
      setFloors(floors.filter((e)=>(e !== item)));
    }else{
      setFloors(state => [...state, item]);
    }
  }

  return (
    <>
        <div className="border-line-bottom"><span></span></div>
        <h5 className="form-title">{getTranslatedText('title.mortagage')}</h5>
        
        <Form.Group>
          <Form.Label>{getTranslatedText('label.building_type')} </Form.Label>
          <div className={`selected-box ${floorOpen ? 'open' : ''}`}>
            <div className="village-box" onClick={iconClick} >
              <Form.Control type="type" value={floors} readOnly placeholder="Select" autoComplete="off" />
              <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleBottom }} />
            </div>
            <div className="search-list-box">
              <ul>
                {floorsData.map((item, i) =>
                  <li key={i}>
                    <Form.Check
                      custom 
                      label={item}
                      type={'checkbox'}
                      id={`custom-inline-${i}`}
                      onClick={() => handleFloorsSelection(item)}
                    />
                  </li>
                )}
              </ul>
            </div>
          </div>
        </Form.Group>
        
        <InputGroupPrepend
            InputGroupName="Sq. yards"
            Input="mortgageArea"
            Label="label.mortagage_area"
            placeholder="Enter Mortgage area"
            ConvertYards={false}
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />
        <Form.Group controlId="ControlSelect5">
            <Form.Label>{getTranslatedText('label.sro_location')}</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Enter SRO Location" 
              name="sroLocation"
              value={values.sroLocation}
              onChange={handleChange}
              onBlur={handleBlur}
            />
            {/* <div className={`selected-box ${searchFive ? 'open' : ''}`}>
              <div className="village-box">
                <Form.Control type="type" value={sro} placeholder="Approved Building Plan" autoComplete="off" />
                <svg className="arrow-right" onClick={iconClickFive} dangerouslySetInnerHTML={{ __html: AngleBottom }} />
              </div>
              <div className="search-list-box">
                <ul>
                  {mandalzoneData.map((item, i) =>
                    <li key={i} onClick={() => { setSro(item), iconClickFive() }}>{item}</li>
                  )}
                </ul>
              </div>
            </div> */}
          </Form.Group>
          
          <Form.Group>
            <Form.Label>{getTranslatedText('label.market_value_title')}</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="₹ 00"
              name="marketValue"
              value={values.marketValue}
              onChange={handleChange}
              onBlur={handleBlur}
            />
          </Form.Group>
    </>
  )
}


export default Mortagage;