import React, { useState, useEffect } from 'react';
import { Form, InputGroup, FormControl, Button } from 'react-bootstrap';
import { AngleRight } from '../../utils/icons';
import { getTranslatedText } from '../../utils/translationUtils';
import InputGroupPrepend from '../../components/common/input-group'
import ContextApi from '../../context'

const AbuttingRoad = ({
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    customHandleChange
}) => {

  const [exSideOne, setExSideOne] = useState(false)
  const [exSideTwo, setExSideTwo] = useState(false)
  const [exSideThree, setExSideThree] = useState(false)
  const [sideOne, setSideOne] = useState(false)
  const [sideTwo, setSideTwo] = useState(false)
  const [sideThree, setSideThree] = useState(false)

  const [totalRoadAffectedArea, setTotalRoadAffectedArea] = useState(0)

  const TotalAffectedNetPlot = () => {
    const TotalAffected = parseInt(values.affectedArea) + parseInt(values.side1affectedArea) + parseInt(values.side2affectedArea) + parseInt(values.side3affectedArea);
    customHandleChange('affectedTotal', TotalAffected);

    const netPlot = parseInt(values.prBuiltup) - TotalAffected
    customHandleChange('netPlotArea', netPlot);
  }

  return (
    <ContextApi.Consumer>
      {value =>
    <>
      <div className="border-line-bottom"><span></span></div>
      {value.bpCaseType === "ADDITIONAL" && 
      <>
      <h5 className="form-title">Existing {getTranslatedText('title.abutting')}(front)</h5>
        <InputGroupPrepend
            InputGroupName="Feets"
            Input="exist_exRoadWidth"
            Label="label.existing_road_width"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />
        <InputGroupPrepend
            InputGroupName="Feets"
            Input="exist_prRoadWidth"
            Label="label.proposed_road"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />
        <InputGroupPrepend
            InputGroupName="Feets"
            Input="exist_affectedRoad"
            Label="label.affected_road"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />
        <InputGroupPrepend
            InputGroupName="Feets"
            Input="exist_affectedArea"
            Label="label.affected_area"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />
      {!exSideOne && <>
        <div className="Abutting-side-block">
          <h5 className="form-title">{getTranslatedText('title.abutting_road_details')}(Side 1)</h5>
          <Button onClick={() => setExSideOne(true)}>
            <span>{getTranslatedText('button.add')}</span>
            <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleRight }} />
          </Button>
        </div>
        <span className="Abutting-dec">Click on (+) button to add new side if applicable</span>
      </>}

      {exSideOne &&
      <>
        <h5 className="form-title">Existing {getTranslatedText('title.abutting')} (side 1)</h5>
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side1exRoadWidth"
              Label="label.existing_road_width"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side1prRoadWidth"
              Label="label.proposed_road"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side1affectedRoad"
              Label="label.affected_road"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side1affectedArea"
              Label="label.affected_area"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />

        <div className="Abutting-side-block">
          <h5 className="form-title">{getTranslatedText('title.abutting_road_details')} (Side 2)</h5>
          <Button onClick={() => setExSideTwo(true)}>
            <span>{getTranslatedText('button.add')}</span>
            <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleRight }} />
          </Button>
        </div>
        <span className="Abutting-dec">Click on (+) button to add new side if applicable</span>
      </>
      }

    {exSideTwo &&
      <>
        <h5 className="form-title">Existing {getTranslatedText('title.abutting')} (side 2)</h5>
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side2exRoadWidth"
              Label="label.existing_road_width"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side2prRoadWidth"
              Label="label.proposed_road"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side2affectedRoad"
              Label="label.affected_road"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side2affectedArea"
              Label="label.affected_area"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />

        <div className="Abutting-side-block">
          <h5 className="form-title">{getTranslatedText('title.abutting_road_details')} (Side 3)</h5>
          <Button onClick={() => setExSideThree(true)}>
            <span>{getTranslatedText('button.add')}</span>
            <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleRight }} />
          </Button>
        </div>
        <span className="Abutting-dec">Click on (+) button to add new side if applicable</span>
      </>
      }
    
    {exSideThree &&
      <>
        <h5 className="form-title">Existing {getTranslatedText('title.abutting')} (side 3)</h5>
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side3exRoadWidth"
              Label="label.existing_road_width"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side3prRoadWidth"
              Label="label.proposed_road"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side3affectedRoad"
              Label="label.affected_road"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="exist_side3affectedArea"
              Label="label.affected_area"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
      </>
      }
        <InputGroupPrepend
            InputGroupName="Feets"
            Input="exist_affectedTotal"
            Label="label.affected_total"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />
        <InputGroupPrepend
            InputGroupName="Feets"
            Input="exist_netPlotArea"
            Label="label.net_plot_area"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />
      </>
    }
    
      <h5 className="form-title">Proposed {getTranslatedText('title.abutting')}</h5>
      <InputGroupPrepend
            InputGroupName="Feets"
            Input="exRoadWidth"
            Label="label.existing_road_width"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />
        <InputGroupPrepend
            InputGroupName="Feets"
            Input="prRoadWidth"
            Label="label.proposed_road"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
        />
        <Form.Group className="document-box">
            <Form.Label htmlFor="inlineFormInputGroupUsername2">{getTranslatedText('label.affected_road')}</Form.Label>
            <InputGroup>
                <InputGroup.Prepend>
                    <InputGroup.Text >Feets</InputGroup.Text>
                </InputGroup.Prepend>
                <div className="west-input">
                    <FormControl 
                        type="text" 
                        name="affectedRoad"
                        readOnly
                        value={values.prRoadWidth === 0 ? 0 :(parseInt(values.prRoadWidth) - parseInt(values.exRoadWidth))/2} 
                    />
                </div>
            </InputGroup>
        </Form.Group>
        <InputGroupPrepend
            InputGroupName="Feets"
            Input="affectedArea"
            Label="label.affected_area"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={(e) => { handleBlur(e), TotalAffectedNetPlot()}}
        />
      {!sideOne && <>
      <div className="Abutting-side-block">
        <h5 className="form-title">{getTranslatedText('title.abutting_road_details')}(Side 1)</h5>
        <Button onClick={() => setSideOne(true)}>
          <span>{getTranslatedText('button.add')}</span>
          <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleRight }} />
        </Button>
      </div>
      <span className="Abutting-dec">Click on (+) button to add new side if applicable</span>
      </>}

      {sideOne &&
      <>
        <h5 className="form-title">Proposed {getTranslatedText('title.abutting')} (side 1)</h5>
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="side1exRoadWidth"
              Label="label.existing_road_width"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="side1prRoadWidth"
              Label="label.proposed_road"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <Form.Group className="document-box">
              <Form.Label htmlFor="inlineFormInputGroupUsername2">{getTranslatedText('label.affected_road')}</Form.Label>
              <InputGroup>
                  <InputGroup.Prepend>
                      <InputGroup.Text >Feets</InputGroup.Text>
                  </InputGroup.Prepend>
                  <div className="west-input">
                      <FormControl 
                          type="text" 
                          name="side1affectedRoad"
                          readOnly
                          value={values.side1prRoadWidth === 0 ? 0 :(parseInt(values.side1prRoadWidth) - parseInt(values.side1exRoadWidth))/2} 
                      />
                  </div>
              </InputGroup>
          </Form.Group>
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="side1affectedArea"
              Label="label.affected_area"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={(e) => { handleBlur(e), TotalAffectedNetPlot()}}
          />
        {!sideTwo &&
        <>
          <div className="Abutting-side-block">
            <h5 className="form-title">{getTranslatedText('title.abutting_road_details')} (Side 2)</h5>
            <Button onClick={() => setSideTwo(true)}>
              <span>{getTranslatedText('button.add')}</span>
              <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleRight }} />
            </Button>
          </div>
          <span className="Abutting-dec">Click on (+) button to add new side if applicable</span>
        </>}
      </>
      }

    {sideTwo &&
      <>
        <h5 className="form-title">Proposed {getTranslatedText('title.abutting')} (side 2)</h5>
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="side2exRoadWidth"
              Label="label.existing_road_width"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="side2prRoadWidth"
              Label="label.proposed_road"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <Form.Group className="document-box">
              <Form.Label htmlFor="inlineFormInputGroupUsername2">{getTranslatedText('label.affected_road')}</Form.Label>
              <InputGroup>
                  <InputGroup.Prepend>
                      <InputGroup.Text >Feets</InputGroup.Text>
                  </InputGroup.Prepend>
                  <div className="west-input">
                      <FormControl 
                          type="text" 
                          name="side2affectedRoad"
                          readOnly
                          value={values.side2prRoadWidth === 0 ? 0 :(parseInt(values.side2prRoadWidth) - parseInt(values.side2exRoadWidth))/2} 
                      />
                  </div>
              </InputGroup>
          </Form.Group>
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="side2affectedArea"
              Label="label.affected_area"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={(e) => { handleBlur(e), TotalAffectedNetPlot()}}
          />
        {!sideThree &&
        <>
          <div className="Abutting-side-block">
            <h5 className="form-title">{getTranslatedText('title.abutting_road_details')} (Side 3)</h5>
            <Button onClick={() => setSideThree(true)}>
              <span>{getTranslatedText('button.add')}</span>
              <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleRight }} />
            </Button>
          </div>
          <span className="Abutting-dec">Click on (+) button to add new side if applicable</span>
        </>}
      </>
      }
    
    {sideThree &&
      <>
        <h5 className="form-title">Proposed {getTranslatedText('title.abutting')} (side 3)</h5>
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="side3exRoadWidth"
              Label="label.existing_road_width"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="side3prRoadWidth"
              Label="label.proposed_road"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
          />
          <Form.Group className="document-box">
              <Form.Label htmlFor="inlineFormInputGroupUsername2">{getTranslatedText('label.affected_road')}</Form.Label>
              <InputGroup>
                  <InputGroup.Prepend>
                      <InputGroup.Text >Feets</InputGroup.Text>
                  </InputGroup.Prepend>
                  <div className="west-input">
                      <FormControl 
                          type="text" 
                          name="side3affectedRoad"
                          readOnly
                          value={values.side3prRoadWidth === 0 ? 0 :(parseInt(values.side3prRoadWidth) - parseInt(values.side3exRoadWidth))/2} 
                      />
                  </div>
              </InputGroup>
          </Form.Group>
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="side3affectedArea"
              Label="label.affected_area"
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={(e) => { handleBlur(e), TotalAffectedNetPlot()}}
          />
      </>
      }
      
        <InputGroupPrepend
            InputGroupName="Feets"
            Input="affectedTotal"
            Label="label.affected_total"
            readOnly={true}
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />
          <InputGroupPrepend
              InputGroupName="Feets"
              Input="netPlotArea"
              Label="label.net_plot_area"
              readOnly={true}
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
            />
    </>
  }
  </ContextApi.Consumer>
  )
}


export default AbuttingRoad;