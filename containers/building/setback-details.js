import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { Form, Button, Col, InputGroup, FormControl } from 'react-bootstrap';
import { AngleRight, AngleBottom, MapIcon, AngleLeft, UploadFile } from '../../utils/icons';
import { getTranslatedText } from '../../utils/translationUtils';
import InputGroupPrepend from '../../components/common/input-group'

import ContextApi from '../../context'

const SetBackDetails = ({
    values,
    errors,
    touched,
    handleChange,
    handleBlur
}) => {

  return (
    <ContextApi.Consumer>
      {value =>
        <>
        <div className="border-line-bottom"><span></span></div>
        {value.bpCaseType === "ADDITIONAL" && 
        <>
          <h5 className="form-title">Existing {getTranslatedText('heading.setback_details')}</h5>
          <InputGroupPrepend
            InputGroupName="Feets"
            Input="exFrontSetback"
            Label="label.front_setback"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />
          <InputGroupPrepend
            InputGroupName="Feets"
            Input="exSideSetback"
            Label="label.other_setback"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />
        </> }
        <h5 className="form-title">Proposed {getTranslatedText('heading.setback_details')}</h5>
          <InputGroupPrepend
            InputGroupName="Feets"
            Input="frontSetback"
            Label="label.front_setback"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />
          <InputGroupPrepend
            InputGroupName="Feets"
            Input="otherSetback"
            Label="label.other_setback"
            values={values}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />
        
    </>
  }
  </ContextApi.Consumer>
  )
}


export default SetBackDetails;