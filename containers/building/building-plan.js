import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { Form, Button, Col, InputGroup, FormControl } from 'react-bootstrap';
import { AngleRight, AngleBottom, MapIcon, AngleLeft, UploadFile } from '../../utils/icons';
import { getTranslatedText } from '../../utils/translationUtils';
import InputGroupPrepend from '../../components/common/input-group'

const BuildingPlan = ({
    values,
    errors,
    touched,
    handleChange,
    handleBlur
}) => {

  return (
    <>
        <div className="border-line-bottom"><span></span></div>
        <h5 className="form-title">{getTranslatedText('label.auto_dcr')}</h5>
        
        <Form.Group className="verify group-block">
            <div className="group">
              <Form.Group>
                <Form.Label>Reference No</Form.Label>
                <Form.Control 
                    type="number" 
                    name="referenceNo"
                    placeholder="Enter Reference No"
                    values={values}
                    errors={errors}
                    touched={touched}
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                />
              </Form.Group>
              <Form.Group className="verify-button">
                <Button > Verify </Button>
              </Form.Group>
            </div>
        </Form.Group>
        
        <Form.Group className="verify group-block">
            <div className="group">
              <Form.Group>
                <Form.Label>Secret Key</Form.Label>
                <Form.Control 
                    type="number" 
                    name="Secret"
                    placeholder="Enter Secret Key"
                    values={values}
                    errors={errors}
                    touched={touched}
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                />
              </Form.Group>
              <Form.Group className="verify-button">
                <Button > Verify </Button>
              </Form.Group>
            </div>
        </Form.Group>
    </>
  )
}


export default BuildingPlan;