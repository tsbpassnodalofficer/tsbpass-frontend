import React, { useState } from 'react';
import Link from 'next/link';
import { Row, Col } from 'react-bootstrap';

function Footer(props) {
   return (
      <footer>
         <div className="footer-block">
            <div className="footer-first-block">
               <Row>
                  <Col md={3}>
                     <div className="footer-about-block">
                        <h4>About US</h4>
                        <ul>
                           <li><a href="#">ContinuingEducation.com</a></li>
                           <li><a href="#">Leadership Team</a></li>
                           <li><a href="#">CE Direct</a></li>
                           <li><a href="#">Accreditation</a></li>
                           <li><a href="#">Write for Us</a></li>
                        </ul>
                     </div>
                  </Col>
                  <Col md={3}>
                     <div className="footer-resources-block">
                        <h4>Resources</h4>
                        <ul>
                           <li><a href="#">CE Course Requirements</a></li>
                           <li><a href="#">CE Course Instructions</a></li>
                           <li><a href="#">Evidence-Based Practice</a></li>
                           <li><a href="#">Free CE Alerts</a></li>
                           <li><a href="#">CE-PRO Membership</a></li>
                           <li><a href="#">First Course Free</a></li>
                        </ul>
                     </div>
                  </Col>
                  <Col md={2}>
                     <div className="footer-support-block">
                        <h4>Support</h4>
                        <ul>
                           <li><a href="#">Help</a></li>
                           <li><a href="#">FAQs</a></li>
                           <li><a href="#">Contact Us</a></li>
                           <li><a href="#">Terms of Service</a></li>
                           <li><a href="#"> Privacy Policy</a></li>
                        </ul>
                     </div>
                  </Col>
                  <Col md={4}>
                     <div className="footer-logo-wrapper">
                        <div className="footer-logo-block">
                           <div className="footer-logo">
                              <img src="html/images/footer/logo.png" alt="" />
                           </div>
                           <ul>
                              <li><a href="#">(877) 200 0020</a></li>
                              <li><a href="#">1010 Sync St</a></li>
                              <li><a href="#">Morrisville, North Carolina 27560</a></li>
                           </ul>
                           <div className="footer-bottom-logo">
                              <img src="html/images/footer/01.png" className="image1" alt="" />
                              <img src="html/images/footer/02.png" className="image2" alt="" />
                              <img src="html/images/footer/03.png" className="image3" alt="" />
                           </div>
                        </div>
                     </div>
                  </Col>
               </Row>
            </div>
            <div className="footer-second-block">
               <span>Terms and Conditions.</span>
               <span>Privacy Statement</span>
               <span>©Relias, Inc. 2019</span>
               <span className="playstore"><img src="html/images/footer/playstore.png" alt="" /></span>
            </div>
            <div className="footer-third-block">
               <span>Copyright ©2020 Telangana State Building Permission Approval and Self Certification System. All Rights Reserved.</span>
            </div>
         </div>
      </footer>
   )
}
export default Footer;