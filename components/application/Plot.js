import React, { useState, useEffect } from 'react';
import { Form, Button, Spinner } from 'react-bootstrap';
import { AngleRight, AngleBottom, MapIcon, AngleLeft, UploadFile } from '../../utils/icons';
import { getTranslatedText } from '../../utils/translationUtils';
import PlotInfo from '../../containers/plot/plot-info'
import PlotAddress from '../../containers/plot/plot-address'
import TaxDetails from '../../containers/plot/Tax-details';
import ScheduleBoundaries from '../../containers/plot/schedule-boundaries'
import SitePhotoGraphs from '../../containers/plot/site-photographs'

import { useFormik } from 'formik';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import axios from 'axios';
import apiConstants from '../../constants/apiConstants';
const apiURL = require('../../config/url-front').API_SERVER;



const SignInSchema = Yup.object().shape({
  plotNo: Yup.string().required('This field is required'),
  streetName: Yup.string().required('This field is required'),
  localityName: Yup.string().required('This field is required'),
  surveyName: Yup.string().required('This field is required'),
  villageName: Yup.string().required('This field is required'),
  plotArea: Yup.number().required('This field is required'),
  plotAreaGround: Yup.number()
  .max(Yup.ref('plotArea'), "Ground should be min of plot area")
  .required('This field is required'),
  plotAddress: Yup.string().required('This field is required'),
  geoLocation: Yup.string().required('This field is required'),
  frontPhoto: Yup.string().required('This field is required'),
  backPhoto: Yup.string().required('This field is required'),
  sidePhoto: Yup.string().required('This field is required'),
  sideTwoPhoto: Yup.string().required('This field is required'),
  // additionalPhoto: Yup.string().required('This field is required'),
});

const PlotDetails = ({
  currentStep,
  changeStep
}) => {

  const [houseType, setHouseType] = useState([])
  const [localityType, setLocalityType] = useState([])
  const [surveyType, setSurveyType] = useState([])
  const [questions, setQuestions] = useState([]);

  useEffect(() => {
    const authToken = localStorage.getItem('auth-token');

    if(authToken && currentStep === 2){
        axios.get(apiURL + apiConstants.PLOT_ENUMS.URL, {
          headers: {
            Authorization: 'Bearer ' + authToken
          }
        })
        .then(function (response) {
          setHouseType(response.data.data.house_no_type);
          setLocalityType(response.data.data.locality_type);
          setSurveyType(response.data.data.plot_land_type);
        })
        .catch(function (error) {
          console.log(error);
        });

        axios.get(apiURL + apiConstants.QUESTIONS.URL, {
          params: {
            category: 'plot'
          },
          headers: {
            'Authorization': 'Bearer ' + authToken,
            'Accept' : '*/*'
          }
        })
        .then(function (response) {
          // Set results state
          setQuestions(response.data.data.questions)
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }, [currentStep])

  const formik = useFormik({
    initialValues:{ 
      plotType: 'Plot_no',
      plotNo: "",
      streetType: 'Street_no',
      streetName: "",
      colonyType: 'Locality',
      localityName: "",
      surveyType: 'Survey_no',
      surveyName: "",
      villageName: "",
      plotAddress: "",
      geoLocation: "",
      plotArea: "",
      plotAreaGround: "",
      frontPhoto: "",
      backPhoto:"",
      sidePhoto:"",
      sideTwoPhoto:"",
      additionalPhoto:"",
      northPlotWidth: 0,
      northRoadwidth: 0,
      southplotWidth: 0,
      southRoadWidth: 0,
      eastRoadWidth: 0,
      eastPlotWidth: 0,
      westRoadWidth: 0,
      westPlotWidth: 0,
      questionAnswers: []
    },
    validationSchema:SignInSchema,
    onSubmit:async (values, { setSubmitting }) => {
        const authToken = localStorage.getItem('auth-token');
        const applicationId = localStorage.getItem('application-id');
        if(authToken && applicationId ){
          const data = {
            "application_id": applicationId,
            "location_id": values.locationId,
            "house_no_type": values.plotType,
            "house_no_value": values.plotNo,
            "locality_type": values.colonyType,
            "locality_value": values.localityName,
            "plot_address": values.plotAddress,
            "geo_coordinates": values.geoLocation.toString(),
            "plot_land_type": values.surveyType,
            "plot_land_value": values.surveyName,
            "plot_area_as_per_document": values.plotArea,
            "actual_plot_area": values.plotAreaGround,
            "building_plan_number": "string",
            "market_value": 0,
            "sro_location": "string",
            "north_plot_width": values.northPlotWidth,
            "north_road_width": values.northRoadwidth,
            "south_plot_width": values.southplotWidth,
            "south_road_width": values.southRoadWidth,
            "east_road_width": values.eastRoadWidth,
            "east_plot_width": values.eastPlotWidth,
            "west_road_width": values.westRoadWidth,
            "west_plot_width": values.westPlotWidth,
            "building_type": "NEW",
            "application_answers": values.questionAnswers
          }
          
          const config = {
            headers: {
              'Authorization': 'Bearer ' + authToken,
              'Accept' : '*/*'
            }
          };
            if(window.confirm('Details once saved cannot be edited. Do you want to continue?')){
              await axios.post(apiURL + apiConstants.PLOT_POST.URL, data, config)
              .then(function (response) {
                // console.log('response', response);
                if(response.data.success){
                  localStorage.setItem('property-id', response.data.data.property.id)
                    changeStep(3);
                }else{
                  toast.error(response.data.message, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                  });
                }
              })
              .catch(function (error) {
                toast.error(error.response.data.message, {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                });
              });
            }
          }else{
            toast.error('Something went wrong', {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
            });
          }
          setTimeout(() => {
            // alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
          }, 400);

        // changeStep(3);
    }
  });

  const customHandleChange = (name, value) => {
    formik.setFieldValue(name, value);
  }

  // console.log('formdata', this.state.values)
  return  (
    <>
      <div className={`personal-details-from ${currentStep === 2 ? `` : `hide-form`}`}>
        <div className="main-title">
          <h3>{getTranslatedText('heading.plot_info')}</h3>
        </div>
          <Form className="login-form-type" onSubmit={formik.handleSubmit} autoComplete="off">
            
            <PlotInfo 
                values={formik.values}
                errors={formik.errors}
                touched={formik.touched}
                handleChange={formik.handleChange}
                handleBlur={formik.handleBlur} 
                customHandleChange={customHandleChange}
                houseType={houseType}
                localityType={localityType}
                surveyType={surveyType}
              />

            <PlotAddress 
                values={formik.values}
                errors={formik.errors}
                touched={formik.touched}
                handleChange={formik.handleChange}
                handleBlur={formik.handleBlur} 
            />

            <ScheduleBoundaries 
                values={formik.values}
                errors={formik.errors}
                touched={formik.touched}
                handleChange={formik.handleChange}
                handleBlur={formik.handleBlur} 
                customHandleChange={customHandleChange}
            />

            <SitePhotoGraphs 
                values={formik.values}
                errors={formik.errors}
                touched={formik.touched}
                handleChange={formik.handleChange}
                handleBlur={formik.handleBlur} 
                customHandleChange={customHandleChange}
            />

            <TaxDetails 
                values={formik.values}
                errors={formik.errors}
                touched={formik.touched}
                handleChange={formik.handleChange}
                handleBlur={formik.handleBlur} 
                customHandleChange={customHandleChange}
                questions={questions}
            />


            {/* <div className="personal-deatails-buttons">
              <div className="back-btn" onClick={() => changeStep(1)}>
                <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleLeft }} />
                <span>{getTranslatedText('button.back')}</span>
              </div> */}
                <Button type="submit" disabled={formik.isSubmitting}>
                  <span>{getTranslatedText('button.save_continue')} </span>
                  {formik.isSubmitting?
                    <Spinner
                      as="span"
                      animation="border"
                      size="sm"
                      role="status"
                      aria-hidden="true"
                    />
                  :
                  <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleRight }} /> }
                </Button>
            {/* </div> */}
          </Form>
      </div>
    </>
  )
}


export default PlotDetails;