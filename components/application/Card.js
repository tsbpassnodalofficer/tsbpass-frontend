import React, { useState, useEffect } from 'react';
import { Container, Form, Button, Card, CardGroup, Modal} from 'react-bootstrap';
import Upload from '../../containers/plot/upload';
import {Logo} from '../../utils/icons';
const Cards = (props) => { 
    const {item} = props;
    const [modalShown, setModalShown] = useState(false);
    const toggleModalVisiblity = () => {
        setModalShown(modalShown ? false : true);
      };
        return (
                   <div style={{ marginTop: 20, marginBottom: 10, width: 1200 }}>
                   <CardGroup>
                       <Card >
                           <Card.Body>
                               <Card.Title style={{fontSize: '12px',color:'#9EABB5' }}>Application ID</Card.Title>
                               <Card.Text>
                               <strong>{item.id}</strong>
                               </Card.Text>
                           </Card.Body>
                       </Card>
                       <Card>
                           <Card.Body>
                               <Card.Title style={{fontSize: '12px',color:'#9EABB5' }}>Application Type :</Card.Title>
                               <Card.Text>
                               <strong>{item.title}</strong>
                               </Card.Text>
                           </Card.Body>
                       </Card>
                       <Card>
                           <Card.Body>
                               <Card.Title style={{fontSize: '12px',color:'#9EABB5' }}>Applied Date :</Card.Title>
                               <Card.Text>
                               <strong>01 July 2020</strong>
                                </Card.Text>
                           </Card.Body>
                       </Card>
                        <Card>
                           <Card.Body>
                               <Card.Text style={{ textAlign: 'Center', marginTop: 54,color:'red' }}>
                                   Incomplete
                               </Card.Text>
                           </Card.Body>
                       </Card>
                        <Card>
                           <Card.Body>
                           <svg style={{marginBottom:-140,marginLeft:-21}} dangerouslySetInnerHTML={{ __html: Logo }} />
                               <Card.Title style={{fontSize: '12px',marginLeft: 10,marginTop: -19 }}><strong>Draft</strong></Card.Title>
                               <Card.Text style={{color:'#9EABB5',marginTop: 5,marginLeft: -15}}>
                               Expires on 
                               </Card.Text>
                               <Card.Text style={{marginLeft: 66,marginTop: -44,fontSize:12}}><strong>15 July 2020</strong></Card.Text>
                           </Card.Body>
                       </Card>
                       <Card>
                           <Card.Body>
                               <Card.Text>
                                <div>
                                   
                               <Button style={{marginRight:23,marginLeft:-10}} onClick={toggleModalVisiblity} variant="outline-success"> <span>&#10514;</span> Upload</Button>{' '}
                               <Modal show={modalShown}>
                                        <Modal.Body style={{marginLeft: 107,marginRight: -68}}>
                                            <div style={{ height: '50%', width: '50%' }}>
                                                <Upload />
                                            </div>
                                        </Modal.Body>
                                        <Modal.Footer style={{paddingRight: 215}}>
                                            <Button onClick={toggleModalVisiblity} className="area-btn"  >Done</Button>
                                        </Modal.Footer>
                                    </Modal>
                               <Button  style={{marginTop: -80,marginRight: 107,marginBottom: -16,marginLeft: 107}}variant="outline-success"><span>&#10515;</span></Button>{' '}
                               
                               </div>
                               </Card.Text>
                           </Card.Body>
                       </Card>
                   </CardGroup>
               </div>
            )
}

export default Cards;