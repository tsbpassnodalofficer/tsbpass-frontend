import React, { useState } from 'react';
import Link from 'next/link';
import { Row, Col, Form, Button,Dropdown, DropdownButton, ButtonGroup} from 'react-bootstrap';
import { AngleRight, Bell, upload, arrow, Logout, Lock, UserProfile } from '../../utils/icons';

import { getTranslatedText } from '../../utils/translationUtils';
import 'react-tabs/style/react-tabs.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../styles/front.css';


const Header = () => {
   const [modalShown, setModalShown] = useState(false);
   const toggleModalVisiblity = () => {
       setModalShown(modalShown ? false : true);
     };
    return(
        <header>
        <div className="logo-menu-block">
           <div className="logo-block">
              <Link href="/">
                 <a>
                    <img src="/html/images/register/logo.png" alt="" />
                 </a>
              </Link>
           </div>
           <div style={{marginLeft:'30%'}}>
           <svg style={{margin:10}} className="arrow-right" dangerouslySetInnerHTML={{ __html: Bell }} />
           <Button  variant="outline-dark">Update Contact Details</Button>{' '}
           </div>
           <Dropdown style={{marginLeft:40}}>
                   <Dropdown.Toggle style={{background: 'white',border: 'unset'}} id="dropdown-basic">
                      <img style={{width: '18%',marginRight: 13}} src="/html/images/register/circle-cropped.png"/>
                      <span style={{color:'black'}}>Sai Kiran</span>
                      <svg style={{height:22,marginRight:-289}} dangerouslySetInnerHTML={{ __html: arrow }}></svg>
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1"><svg style={{height: 13,marginRight: -131,marginLeft: -145}} dangerouslySetInnerHTML={{ __html: UserProfile }}>
                         </svg>View Profile</Dropdown.Item>
                      <Dropdown.Item href="#/action-2"><svg style={{height:18, marginRight: -276}} dangerouslySetInnerHTML={{ __html: Lock }}>
                         </svg>Change Password</Dropdown.Item>
                      <Dropdown.Item href="#/action-3"><svg style={{height: 20,marginTop: 7,marginRight: -276}} dangerouslySetInnerHTML={{ __html: Logout }}></svg>Logout</Dropdown.Item>
                   </Dropdown.Menu>
                </Dropdown>
 
     </div>
     </header>
    )
}


export default Header;