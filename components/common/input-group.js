import React, { useState } from 'react';
import Link from 'next/link';
import { Row, Col, Form, InputGroup, FormControl } from 'react-bootstrap';
import { AngleRight, AngleBottom } from '../../utils/icons';

import { convertToSqMeters } from '../../utils/convertionUtils'
import { getTranslatedText } from '../../utils/translationUtils';

import ContextApi from '../../context'

const InputGroupPrepend = ({
    List,
    readOnly=false,
    Dropdown=false,
    ConvertYards=false,
    InputGroupName,
    Input,
    Label,
    values,
    errors,
    touched,
    handleChange,
    handleBlur
}) => {
    const [type, setType] = useState('')
    const [meters, SetMeters] = useState(0)
    const convertedMeters = (sq, value) => {
        const meters = convertToSqMeters(sq);
        if(Input === 'plotAreaGround'){
            value.updatePlotArea(meters)
            localStorage.setItem('plot-area', meters);
        }
    
        SetMeters(meters)
    }
    return(
        <ContextApi.Consumer>
          {value =>
            <Form.Group className="document-box">
                <Form.Label htmlFor="inlineFormInputGroupUsername2">{getTranslatedText(Label)}</Form.Label>
                <InputGroup>
                    <InputGroup.Prepend>
                        {Dropdown ?
                        <div className="selected-box">
                            <Form.Control as="select"
                                name={InputGroupName}
                                defaultValue={type}
                                onChange={handleChange}
                            >
                                <option>{List.length > 0 ? 'Select Type': 'Loading...'}</option>
                                {List.length > 0 && List.map((item, index) => 
                                    <option onClick={(e) => setType(e.target.value)} key={`plot-types-${index}`}>{item}</option>
                                )}
                            </Form.Control>
                            <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleBottom }} />
                        </div>
                        :
                        <InputGroup.Text >{InputGroupName}</InputGroup.Text> }
                    </InputGroup.Prepend>
                    <div className="west-input">
                        <FormControl 
                            type="text" 
                            name={Input}
                            readOnly={readOnly}
                            className={errors[Input] && touched[Input] && 'has-error'}
                            onChange={handleChange}
                            onBlur={(e) => {handleBlur(e), ConvertYards && convertedMeters(e.target.value, value) }}
                            value={values && values[Input]} 
                        />
                        {ConvertYards && <span>(Sq. mts {meters})</span> }
                    </div>
                </InputGroup>
                {errors[Input] && touched[Input] && <p>{errors[Input]}</p>}
            </Form.Group>
        }
        </ContextApi.Consumer>
    )
}


export default InputGroupPrepend;