import React, { useState } from 'react';
import Link from 'next/link';
import { Form } from 'react-bootstrap';
import FileUpload from '../../components/common/file-upload'
import { AngleRight, AngleBottom } from '../../utils/icons';

import { getTranslatedText } from '../../utils/translationUtils';

const QuestionsInput = ({
    item,
    errors,
    touched,
    handleBlur,
    customHandleChange
}) => {
    
    const [radio, setRadio] = useState(false)
    const [searchFour, setSearchFour] = useState(false);
    const [plan, setPlan] = useState({id:0, name: ''});
    const [caseType, setCaseType] = useState("NEW");

    const iconClickFour = () => {
        setSearchFour(!searchFour)
    }

    const selectedPlanFields = (plan) => {
        if(plan.options && plan.options.length > 0){
          return plan.options.map((item, index) => {
          return ( <>
            {item.response_type === 'text' &&
              <Form.Group>
                  <Form.Label>{item.question_name}</Form.Label>
                  <Form.Control 
                    type="text" 
                    key={`plotPartOfkey-${index}`}
                    defaultValue=""
                    required
                    placeholder={item.question_name}
                    onBlur={(e) => customHandleChange(item.id, e.target.value)}
                />
              </Form.Group> }
            {item.response_type === 'file_url' &&
            <>
                <Form.Label>{item.question_name}</Form.Label>
                <FileUpload 
                  key={`plotPartOfkey-${index}`}
                  Label={item.question_name}
                  ID={`plotPartOf-${index}`}
                  Filename={`question-${index}`}
                  FileType={item.question_name.split(' ').join('_')}
                  errors={errors}
                  required
                  touched={touched}
                  handleBlur={handleBlur}
                  customHandleChange={(name, value) => customHandleChange(item.id, value)}
                />
            </> }
          </> )
          })
        }
    }

    return(
        <>
            <Form.Group>
                <Form.Label>{item.question_name}</Form.Label>
                {item.response_type === "boolean" && 
                <div className="property-radio">
                    <Form.Check
                    type="radio"
                    label={getTranslatedText('label.yes_label')}
                    name={`property-${item.id}`}
                    id={`property-${item.id}`}
                    onChange={() => {setRadio(true), customHandleChange(item.id, 'TRUE')}}
                    />
                    <Form.Check
                    type="radio"
                    label={getTranslatedText('label.no_label')}
                    name={`property-${item.id}`}
                    id={`property-${item.id}`}
                    onChange={() => {setRadio(false), customHandleChange(item.id, 'FALSE')}}
                    /> 
                </div> } 
                {item.response_type === "dropdown" && 
                <div className="mandalzone-file">
                    <div className={`selected-box ${searchFour ? 'open' : ''}`}>
                        <div className="village-box" onClick={iconClickFour}>
                        <Form.Control type="type" value={plan.option_name} readOnly placeholder="Approved Building Plan" autoComplete="off" />
                        <svg className="arrow-right"  dangerouslySetInnerHTML={{ __html: AngleBottom }} />
                        </div>
                        <div className="search-list-box">
                        <ul>
                            {item.child.map((option, i) =>
                            <li key={`options-${i}`} onClick={() => { setPlan(option), iconClickFour(), customHandleChange(item.id, option.option_name) }}>{option.option_name}</li>
                            )}
                        </ul>
                        </div>
                    </div>
                    <br />
                    {selectedPlanFields(plan)}
                    <span className="map-error-message">Please upload approved building plan</span>
                </div>  }
            </Form.Group>
            
            <Form.Group>
            {(item.response_type === "boolean" && item.child.length > 0  && radio) && 
                item.child.map((child, i) => {
                return (<>
                    <Form.Label>{child.question_name}</Form.Label>
                    <FileUpload 
                        key={`file-uploads-${i}`}
                        Label="Upload the document"
                        ID={`Uploadfileq-${i}`}
                        Filename={`question-${i}`}
                        FileType={item.question_name.split(' ').join('_')}
                        errors={errors}
                        touched={touched}
                        handleBlur={handleBlur}
                        customHandleChange={(name, value) => customHandleChange(child.id, value)}
                    />
                </> )
                }
            )}
            </Form.Group>
        </>
    )
}


export default QuestionsInput;