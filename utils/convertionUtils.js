/**
 * @param {Number} value  value in sq.yards
 */
const convertToSqMeters = value => {
  return parseFloat(value / 1.196).toFixed(2);
};

/**
 * @param {Number} value  value in sq.meters
 */
const convertToSqYards = value => {
  if (value) return parseFloat(value * 1.196).toFixed(2);
  return 0;
};

const convertStringToFloat = floatValue => {
  let value = parseFloat(floatValue);
  if (isNaN(value)) return 0;
  return value;
};

export { convertStringToFloat, convertToSqMeters, convertToSqYards };
