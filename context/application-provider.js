import React, { Component } from 'react'

import ContextApi from './';

class ApplicationProvider extends Component {
    state = {
        heightInMeters: 7,
        plotArea: 250,
        applicationType: 'REG',
        bpCaseType: 'NEW',
        floorsID: 0,
        noOfFloors: 0,
        foolrsType: 'Ground',
        totalBuildupArea: 0,
        totalRoadAffected: 0,
        netPlotArea: 0
    };

    handleCaseType = (type) => {
        this.setState({
            bpCaseType: type
        })
    }

    updateFloors = (ID, floors, type) => {
        this.setState({
            floorsID: ID,
            noOfFloors: floors,
            foolrsType: type,
        })
    }

    updateAppType = (height, area) => {
        var type = this.state.applicationType;


        if(area <= 63 && height <= 7){
            type = 'REG'
        }
        if((area > 63 && area <= 500 )&& height <= 7){
            type = 'SC'
        }
        if((area > 50 && area <= 500 )&& (height > 7 && height <= 10)){
            type = 'SCMOR'
        }
        if(height > 10 || area > 500){
            type = 'SW'
        }
        
        this.setState({
            heightInMeters: height,
            plotArea: area,
            applicationType: type
        })
    }

    render() {
        const { heightInMeters, plotArea, applicationType, floorsID, noOfFloors, foolrsType } = this.state;
        return (
            <ContextApi.Provider
                value={{
                    heightInMeters: heightInMeters,
                    plotArea: plotArea,
                    applicationType: applicationType,
                    floorsID: floorsID,
                    noOfFloors: noOfFloors,
                    foolrsType: foolrsType,
                    updateHeight: (height) => {
                        this.updateAppType(height, plotArea)
                    },
                    updatePlotArea: (area) => {
                        this.updateAppType(heightInMeters, area)
                    },
                    handleContextCaseType: (type) => {
                        this.handleCaseType(type)
                    },
                    updateFloors: (ID, floors, type) => {
                        this.updateFloors(ID, floors, type)
                    }
                }}
            >
                {this.props.children}
            </ContextApi.Provider>
        );
    }
}

export default ApplicationProvider;