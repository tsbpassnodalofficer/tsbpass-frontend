import React, { useState, useEffect } from 'react';
import { Component } from 'react';
import Link from 'next/link';
import { Container, Form, Button, Card, CardGroup} from 'react-bootstrap';
import { AngleRight } from '../../utils/icons';
import Header from '../../components/common/Header1'
import Footer from '../../components/Footers/index';
import ApplicationProvider from '../../context/application-provider'
import { getTranslatedText } from '../../utils/translationUtils';

import Cards from '../../components/application/Card'
import axios from 'axios';
import 'react-tabs/style/react-tabs.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../styles/front.css';


const Dashboard = ({
    currentStep,
  changeStep
}) => {
  const [items, setItems] = useState([]);
  
  useEffect(() => {
    axios.get("https://jsonplaceholder.typicode.com/posts")
        .then(resp => {
			setItems(resp.data);
		})
		.catch(function (error) {
			console.log(error);
		  }); 
  }, [])

	return (
		<div>
			<Header />
			<Container style={{ marginTop: 10, marginBottom: 10, width: 1240 }}>
			<h4 style={{fontSize:'20px'}}><strong>Applications</strong></h4>
			<Button type="submit" style={{ float: 'right'}} >
                <span>{getTranslatedText('button.apply_new')} </span>
              </Button>
			<h6 style={{ marginTop: 30, marginBottom: 10,fontSize:'14px',color:'#9EABB5'}}>Applied Recent</h6>
			{items.map(item => (<Cards key={item.id} item={item} />))}
			</Container>
			<Footer />
		</div>
	)
}
export default Dashboard;