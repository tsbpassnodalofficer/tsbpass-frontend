import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { Container, Form, Button } from 'react-bootstrap';
import { AngleRight } from '../../utils/icons';
import Header from '../../components/common/Header';
import ApplicationProvider from '../../context/application-provider';
import { getTranslatedText } from '../../utils/translationUtils';

import Applicant from '../../components/application/Applicant';
import PlotDetails from '../../components/application/Plot';
import BuildingDetails from '../../components/application/Building';
import VicinityDetails from '../../components/application/Vicinity';
import ReviewSubmit from '../../components/application/Review';

import 'react-tabs/style/react-tabs.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../styles/front.css';


function ApplicationForm(props) {
  const [currentStep, setCurrentStep] = useState(1)
  const [plotArea, setPlotArea] = useState(0)
  
  useEffect(() => {
    window.scrollTo(0, 0);
    setPlotArea(localStorage.getItem('plot-area'))
  }, [currentStep])
  

  return (
    <ApplicationProvider>
      <Header />
      <section className="personal-details-wrapper">
        <Container>
          <div className="personal-details-block p-65">

            <div className="details-tabs-wrapper">
              <ul>
                <li className={currentStep > 1 ? `completed`: 'active'}><span>{getTranslatedText('heading.application')}</span><p></p></li>
                <li className={currentStep === 2 ? `active`: currentStep > 2 ? `completed`: ''}><span>{getTranslatedText('heading.plot')}</span><p></p></li>
                <li className={currentStep === 3 ? `active`: currentStep > 3 ? `completed`: ''}><span>{getTranslatedText('heading.building')}</span><p></p></li>
                {/* {plotArea >= 63 && */}
                <li className={currentStep === 4 ? `active`: currentStep > 4 ? `completed`: ''}><span>{getTranslatedText('heading.vicinity')}</span><p></p></li> 
                {/*  } */}
                <li className={currentStep === 5 ? `active`: currentStep > 5 ? `completed`: ''}><span>{getTranslatedText('heading.review')}</span><p></p></li>
              </ul>
            </div>

            <Applicant 
              currentStep={currentStep} 
              changeStep={(value) => setCurrentStep(value)}
            />

            <PlotDetails 
              currentStep={currentStep} 
              changeStep={(value) => setCurrentStep(value)}
            />

            <BuildingDetails 
              currentStep={currentStep} 
              changeStep={(value) => setCurrentStep(value)}
            />

            <VicinityDetails 
              currentStep={currentStep} 
              changeStep={(value) => setCurrentStep(value)}
            />

            <ReviewSubmit 
              currentStep={currentStep} 
              changeStep={(value) => setCurrentStep(value)}
            />
            
          </div>
        </Container>
      </section>
    </ApplicationProvider>
  );
}

export default ApplicationForm;
