import React, { useState } from 'react';
import Link from 'next/link';
import { Container, Button, Table } from 'react-bootstrap';
import Header from '../../components/common/Header';
import { ArrowLeft, AngleRight, Check, Print } from "../../utils/icons";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../styles/front.css';



function Payment(props) {

  const [paid, setPaid] = useState(false);
  
   return (
      <div className="payment-methods">
         <Header />
         {!paid?
         <div className="payment-wrapper">
            <Container>
               <div className="payment-box">
                  <div className="payment-title">
                     <div className="main-title">
                        <h3>Payment</h3>
                        <h6>
                           <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: ArrowLeft }} />
                           Back To Application</h6>
                     </div>
                     <h5>Application ID : <span>2699/ADIL/0146/2020</span></h5>
                  </div>
                  <div className="payment-content-block">
                     <div className="payment-table mb-0">
                        <div className="payment-first-block">
                           <tr>
                              <th>Fee Description</th>
                              <th>Amount</th>
                           </tr>
                        </div>
                        <div className="payment-second-block">
                           <tr>
                              <td>Conversion Charges Conversion Charges U/s 172 of TMAct 2019</td>
                              <td>₹ 1,000.00</td>
                           </tr>
                           <tr>
                              <td>Environmental Impact Fee Environmental Impact Fee as per GO. 8 & 11- Issued by Mines Department - For Above Sft. 10,000 BUA including Stilt</td>
                              <td>₹ 0.00</td>
                           </tr>
                           <tr>
                              <td>Building Permit Fee</td>
                              <td>₹ 0.00</td>
                           </tr>
                           <tr>
                              <td>Approval of Site: Sub Division of Plot/Amalgamation of Plot Sub Division of Plot/Amalgamation of Plot (Read:- As per amendment No.14 of G.O.Ms.No.7 MA & UD, dt:05.01.16)</td>
                              <td>₹ 2,400.00</td>
                           </tr>
                           <tr>
                              <td>Approval of Site: Betterment Charges As per Section 172 of TM Act 2019 Section 172 of TM Act 2019 Betterment Charges As per Section 172 of TM Act 2019 Charges applicable on Net Plot Area for Plots where complete infrastructure is not provided by the layout owner.</td>
                              <td>₹ 15,000.00</td>
                           </tr>
                           <tr>
                              <td>Development Charges: Built up Area</td>
                              <td>₹ 0.00</td>
                           </tr>
                           <tr>
                              <td>Rain Water Harvesting Charges Rain Water Harvesting Charges As per GO.Ms No.350 MA., dt.9.6.2000 and WALTA Act 2002 On Net Plot area</td>
                              <td>₹ 1,000.00</td>
                           </tr>
                           <tr>
                              <td>Debris removal Charges</td>
                              <td>₹ 1,000.00</td>
                           </tr>
                           <tr>
                              <td>Postage/ Advertisement Charges</td>
                              <td>₹ 1,000.00</td>
                           </tr>
                           <tr>
                              <td>Compound wall permit fee</td>
                              <td>₹ 1,000.00</td>
                           </tr>
                           <tr>
                              <td>Labour Cess <span>Labour Cess (taken in per sq ft) : Up to 10 lakhs for construction budget - GO. 111 & 112 - 15/12/2019 - If more than 10L</span></td>
                              <td>₹ 1,000.00</td>
                           </tr>
                           <tr>
                              <td>DTCP User Charges <span>DTCP user charges as per GO Ms. No. 125 MA dated (Above 200 Sq Mts - 10,000 Flat - Scrutiny fee</span></td>
                              <td>₹ 1,000.00</td>
                           </tr>
                           <tr>
                              <td>Vacant Land tax (VLT)</td>
                              <td>₹ 1,000.00</td>
                           </tr>
                           <tr>
                              <td>Development Charges: Land</td>
                              <td>₹ 1,000.00</td>
                           </tr>
                           <tr>
                              <td>T S-BPass User Charges <span>TS-BPASS User Charges for software development </span></td>
                              <td>₹ 1,000.00</td>
                           </tr>
                        </div>
                     </div>
                     <div className="payment-third-block">
                        <h5>Total Amount to be paid :<span>₹ 25,250.00</span></h5>
                        <Button onClick={() => setPaid(true) } >
                           <span>Proceed to Pay</span>
                           <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleRight }} />
                        </Button>
                     </div>
                  </div>
               </div>
            </Container>
         </div>
         :
         <div className="payment-successful-wrapper p-65">
            <div className="payment-print-btn">
               <Button>
                  <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: Print }} />
                  <span>Print</span>
               </Button>
            </div>
            <div className="payment-successful-box">
               <div className="payment-successful-icon">
                  <svg className="check-icon" dangerouslySetInnerHTML={{ __html: Check }} />
                  <h5>Payment Successful</h5>
                  <h6>Transaction Id: <span>TSB154896456485 </span></h6>
               </div>
               <div className="payment-successful-content">
                  <Table>
                     <tbody>
                        <tr>
                           <td>Paid to :</td>
                           <td>Application ID : 2699/ADIL/0146/2020 <span>Saikiran K</span></td>
                        </tr>
                        <tr>
                           <td>Amount :</td>
                           <td className="payment">₹ 25,250.00</td>
                        </tr>
                        <tr className="card-details">
                           <td>Payment Mode :</td>
                           <td><img
                              src='/html/images/payment/visa.png'
                           />
                              <p className="card-number">
                                 <span>Credit Card </span>xxxx xxxx xxxx x970</p>
                           </td>
                        </tr>
                     </tbody>
                  </Table>
               </div>
               <Button>
                  <span>Done</span>
                  <svg className="arrow-right" dangerouslySetInnerHTML={{ __html: AngleRight }} />
               </Button>
            </div>
         </div>
        }
      </div>
   );
}

export default Payment;