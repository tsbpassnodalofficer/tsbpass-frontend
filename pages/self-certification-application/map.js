import React from 'react';
import {
  Button,
  Modal,
  ModalBody
} from 'shards-react';
import { Col, Row, Spinner } from 'react-bootstrap';
import CustomGoogleMap from './google_map';

export class MapContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      position:  { 
        lat: '17.3850', 
        lng: '78.4867' 
      }
    };
  }

  // componentWillMount() {
  //   if (navigator.geolocation) {
  //   navigator.geolocation.getCurrentPosition(this.showPosition);
  //   }
  // }

  showPosition =(position) =>{
    this.setState({position: {lat: position.lat, lng: position.lng }})
  }

  // onMarkerClick = (t, map, coord) => {
  //   const { latLng } = coord;
  //   const lat = latLng.lat();
  //   const lng = latLng.lng();
  //   this.setState({
  //       position: {lat: lat, lng: lng }
  //     });
  // }

  selectLocation = () => {
    const {  position } = this.state
    const { lat, lng } = position;
    const link = `http://maps.google.com/maps?q=${lat},${lng}`;
    this.props.selectLocation(link);
  }

  render() {
    const { position } = this.state;
    return (
      <Modal 
      open={this.props.openMap}
      onClick={this.onClick}
      className='modal-dialog modal-dialog-centered map-size'
      >
        <ModalBody>
          <Row>
          <Col 
            xs='12'
            md='11'
          
          >
            <div style={{ marginBottom: '100px' }}>
				<CustomGoogleMap
					google={this.props.google}
					center={{lat: 18.5204, lng: 73.8567}}
          height='400px'
          width='600px'
          zoom={15}
          showPosition={this.showPosition}
				/>
</div>
            </Col>
          </Row> 
          <Row>
          <Col
            xs='12'
            md='4'
          >
          <Button
          theme='success'
          type='button'
          onClick={this.selectLocation}
          >
          Submit
          </Button>
          </Col>
          </Row>
        </ModalBody>
      </Modal>
      
    );
  }
}
 
export default MapContainer;